﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FacebookDemo : Singleton<FacebookDemo>
{

    // Use this for initialization
    void Awake()
    {
        InitFB();
    }

    void InitFB()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            FB.Init(InitCallback, OnHideUnity);
        }
    }

    void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to initialize the Facebook SDK");
        }
    }

    void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }


    /* Ham su dung de push event analytic len server facebook*/

    public void PushEvent(string key, float value = 1, Dictionary<string,object> param = null)
    {
        if (FB.IsInitialized)
        {
            FB.LogAppEvent(key, value, param);
        }
    }
}
