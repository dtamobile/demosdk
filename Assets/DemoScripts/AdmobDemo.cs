﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdmobDemo : MonoBehaviour
{
    InterstitialAd ad;
    BannerView bannerView;
    // Use this for initialization

    //Can thay ID
    string androidInterstitial = "ANDROID_INTER_ID";
    string iosInterstitial = "IOS_INTER_ID";
    string androidBanner = "ANDROID_BANNER_ID";
    string iosBanner = "IOS_BANNER_ID";
	
    bool isAcceptConsent;
    Action closeCallback;
	
    //GDPR cho cac nuoc EU
    public void SetConsent(bool isAcceptConsent)
    {
        this.isAcceptConsent = isAcceptConsent;
    }

    #region Full Ad

    public void LoadFullAds()
    {
        #if UNITY_ANDROID
        string adUnitId = androidInterstitial;
#elif UNITY_IOS
        string adUnitId = iosInterstitial;
#else
        string adUnitId = "unexpected_platform";
#endif

        ad = new InterstitialAd(adUnitId);
        ad.OnAdClosed += HandleOnAdClosed;
        AdRequest request;
        if (isAcceptConsent)
        {
            request = new AdRequest.Builder().Build();
        }
        else
        {
            request = new AdRequest.Builder().AddExtra("npa", "1").Build();
        }
        // Load the interstitial with the request.
        ad.LoadAd(request);
    }

    public void ShowFullAds(Action closeCallback)
    {
        this.closeCallback = closeCallback;
        if (IsFullAdsLoaded())
        {
            ad.Show();
        }
        else
        {
            if (closeCallback != null)
            {
                closeCallback.Invoke();
            }
        }
        LoadFullAds();
    }

    public bool IsFullAdsLoaded()
    {
        if (ad == null)
            return false;
        return ad.IsLoaded();
    }

    void HandleOnAdClosed(object sender, EventArgs args)
    {
        if (closeCallback != null)
        {
            closeCallback.Invoke();
        }
    }

    #endregion

    #region Banner

    public void LoadBannerBot()
    {
#if UNITY_ANDROID
        string adUnitId = androidBanner;
#elif UNITY_IPHONE
            string adUnitId = iosBanner;
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        // Create an empty ad request.
        AdRequest request;
        if (isAcceptConsent)
        {
            request = new AdRequest.Builder().Build();
        }
        else
        {
            request = new AdRequest.Builder().AddExtra("npa", "1").Build();
        }

        // Load the banner with the request.
        bannerView.LoadAd(request);

        Debug.Log("Load banner appotax");
        HideBanner();
    }

    public void HideBanner()
    {
        if (bannerView != null)
        {
            bannerView.Hide();
        }
    }

    public void ShowBanner()
    {
        if (bannerView != null)
        {
            bannerView.Show();
        }
    }

    #endregion
}
