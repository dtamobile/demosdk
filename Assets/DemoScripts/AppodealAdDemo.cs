﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;

public class AppodealAdDemo : MonoBehaviour,IInterstitialAdListener,IRewardedVideoAdListener,IBannerAdListener
{

    string androidID = "ANDROID_ID";
    string iosID = "IOS_ID";
    Action closeCallback;
    Action rewardCallback;
    Action skipCallback;

    public void InitAppodeal(bool isAcceptConsent)
    {
        #if UNITY_ANDROID
        string appKey = androidID;
#elif UNITY_IOS
            string appKey = iosID;
#else
            string appKey = androidID;
#endif

        Appodeal.disableNetwork("mailru");
        Appodeal.disableNetwork("adcolony");
        Appodeal.disableNetwork("startapp");
        Appodeal.disableNetwork("yandex");
        Appodeal.disableNetwork("mobvista");

        Appodeal.setAutoCache(Appodeal.INTERSTITIAL, true);
        Appodeal.setAutoCache(Appodeal.BANNER, true);
        Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, true);
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER | Appodeal.REWARDED_VIDEO, isAcceptConsent);
        Appodeal.setInterstitialCallbacks(this);
        Appodeal.setRewardedVideoCallbacks(this);
        Appodeal.setBannerCallbacks(this);
    }

	
    public void ShowFullAds(Action closeCallback = null)
    {
        this.closeCallback = closeCallback;
        if (Appodeal.isLoaded(Appodeal.INTERSTITIAL))
        {
            Appodeal.show(Appodeal.INTERSTITIAL);
        }
    }

    public void ShowRewardAds(Action closeRewardCallback = null, Action skipRewardCallback = null)
    {
        rewardCallback = closeRewardCallback;
        skipCallback = skipRewardCallback;
        if (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
        {
            Debug.Log("Show appodeal");
            Appodeal.show(Appodeal.REWARDED_VIDEO);
        }
    }

    public void ShowBannerBot()
    {
        Appodeal.show(Appodeal.BANNER_BOTTOM);
    }

    public void HideBannerBot()
    {
        Appodeal.hide(Appodeal.BANNER_BOTTOM);
    }

    public void ShowBannerTop()
    {
        Appodeal.show(Appodeal.BANNER_TOP);
    }

    public void HideBannerTop()
    {
        Appodeal.hide(Appodeal.BANNER_TOP);
    }

    public bool IsFullAdsLoaded()
    {
        return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
    }

    public bool IsRewardAdsLoaded()
    {
        return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
    }

    #region IRewardedVideoAdListener implementation

    public void onRewardedVideoLoaded(bool precache)
    {
        
    }

    public void onRewardedVideoFailedToLoad()
    {
        
    }

    public void onRewardedVideoShown()
    {
        
    }

    public void onRewardedVideoFinished(double amount, string name)
    {
        
    }

    public void onRewardedVideoClosed(bool finished)
    {
        if (finished)
        {
            if (rewardCallback != null)
            {
                rewardCallback.Invoke();
            }
        }
        else
        {
            if (skipCallback != null)
            {
                skipCallback.Invoke();
            }
        }
    }

    public void onRewardedVideoExpired()
    {
        
    }

    #endregion

    #region IInterstitialAdListener implementation

    public void onInterstitialLoaded(bool isPrecache)
    {
        
    }

    public void onInterstitialFailedToLoad()
    {
        
    }

    public void onInterstitialShown()
    {
        
    }

    public void onInterstitialClosed()
    {
        if (closeCallback != null)
        {
            closeCallback.Invoke();
        }
    }

    public void onInterstitialClicked()
    {
        
    }

    public void onInterstitialExpired()
    {
        
    }

    #endregion

    #region IBannerAdListener implementation

    public void onBannerLoaded(bool isPrecache)
    {

    }

    public void onBannerFailedToLoad()
    {

    }

    public void onBannerShown()
    {

    }

    public void onBannerClicked()
    {

    }

    public void onBannerExpired()
    {

    }

    #endregion
}
