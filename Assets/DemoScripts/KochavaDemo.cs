﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KochavaDemo : Singleton<KochavaDemo>
{
    //Can dien android ID
    string androidID = "ANDROID_ID";

    //Can dien ios ID
    string iosID = "IOS_ID";

    void Awake()
    {
        InitKochava();
    }

    void InitKochava()
    {
        #if UNITY_ANDROID
        Kochava.Tracker.Config.SetAppGuid(androidID);
#endif
#if UNITY_IOS
        Kochava.Tracker.Config.SetAppGuid(iosID);
#endif
        Kochava.Tracker.Config.SetLogLevel(Kochava.DebugLogLevel.info);
        Kochava.Tracker.Config.SetRetrieveAttribution(true);
        Kochava.Tracker.Initialize();
    }
}
