﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShowAdDemo : Singleton<ShowAdDemo>
{

    AdmobDemo admob;
    AppodealAdDemo appodeal;

    //Init function, need call
    void Awake()
    {
        admob = new AdmobDemo();
        appodeal = new AppodealAdDemo();

        admob.SetConsent(true);
        admob.LoadFullAds();
        appodeal.InitAppodeal(true);
    }


    public void ShowAppodealFirst(Action closeCallback = null)
    {
        if (appodeal.IsFullAdsLoaded())
        {
            appodeal.ShowFullAds(closeCallback);
        }
        else if (admob.IsFullAdsLoaded())
        {
            admob.ShowFullAds(closeCallback);
        }
        else
        {
            admob.LoadFullAds();
            if (closeCallback != null)
            {
                closeCallback.Invoke();
            }
        }
    }

    public void ShowAdmobFirst(Action closeCallback = null)
    {
        if (admob.IsFullAdsLoaded())
        {
            admob.ShowFullAds(closeCallback);
        }
        else if (appodeal.IsFullAdsLoaded())
        {
            admob.LoadFullAds();
            appodeal.ShowFullAds(closeCallback);
        }
        else
        {
            admob.LoadFullAds();
            if (closeCallback != null)
            {
                closeCallback.Invoke();
            }
        }
    }

    public void ShowReward(Action closeCallback = null, Action skipCallback = null)
    {
        if (appodeal.IsRewardAdsLoaded())
        {
            appodeal.ShowRewardAds(closeCallback, skipCallback);
        }
    }

    public void ShowBanner()
    {
        appodeal.ShowBannerBot();
    }

    public void HideBanner()
    {
        appodeal.HideBannerBot();
    }
}
