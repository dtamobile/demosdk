﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleFunction : MonoBehaviour
{

    public void ShowAdsWhenGameOver()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            ShowAdDemo.instance.ShowAppodealFirst(() =>
                {
                    // Show game over ui, ...
                });
        }
        else
        {
            ShowAdDemo.instance.ShowAdmobFirst(() =>
                {
                    // Show game over ui, ...
                });
        }

    }

    //Event log user click play
    public void LogPlayEvent()
    {
        FacebookDemo.instance.PushEvent("Play", 1);
    }
}
